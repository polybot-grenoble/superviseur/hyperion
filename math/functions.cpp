#include "functions.hpp"

// #####################   Point3f   #####################

Point3f::Point3f(){};

Point3f::Point3f(double x, double y, double z){
    this->x = x;
    this->y = y;
    this->z = z;
};

ostream& operator<<(ostream& os, const Point3f& pt)
{
    os << "XYZ : [" << pt.x << "," << pt.y << "," << pt.z << "]";
    return os;
}

bool Point3f::operator==(const Point3f& pt){
    if(x == pt.x && y == pt.y && z == pt.z)
        return true;
    return false;
}

// #####################    Color    #####################

Color::Color(){};

Color::Color(double h, double s, double v){
    this->h = h;
    this->s = s;
    this->v = v;
};

ostream& operator<<(ostream& os, const Color& col)
{
    os << "HSV : [" << col.h << "," << col.s << "," << col.v << "]";
    return os;
}

// #####################    Other    #####################

double modulo(double x, double y){
    int x_int = floor(x/y);
    return x - x_int*y;    
}
