#ifndef __HYPERION_MATH_FUNCTIONS__
#define __HYPERION_MATH_FUNCTIONS__

#include <iostream>
#include <math.h>

using namespace std;

class Point3f{
    public:
        double x, y, z;

        Point3f();
        Point3f(double x, double y, double z);
        friend ostream& operator<<(ostream& os, const Point3f& pt);
        bool operator==(const Point3f& pt);
};

class Color{
    public:
        double h, s, v;

        Color();
        Color(double h, double s, double v);    
        friend ostream& operator<<(ostream& os, const Color& col);
};

/**
 * @brief Return the remainder of the division of x by y
 * @param x dividend
 * @param y divisor
 */
double modulo(double x, double y);

#endif