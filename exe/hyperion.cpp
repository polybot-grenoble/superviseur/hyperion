#include <chrono>
#include <iostream>
#include <thread>
#include "effects_global.hpp"
#include "effects.hpp"

using namespace std;

void sleeper(std::chrono::microseconds us){
    auto start = std::chrono::high_resolution_clock::now();
    auto end = start + us;
    do{
        std::this_thread::yield();
    }while(std::chrono::high_resolution_clock::now() < end);
}

enum{
    VERTICAL_RIGHT = 0,
    VERTICAL_LEFT,
    HORIZONTAL_RIGHT,
    HORIZONTAL_MIDDLE,
    HORIZONTAL_LEFT,
    MIDDLE_RIGHT,
    MIDDLE_LEFT
};

int main(void) {
    Segment seg1 = Segment(0, 93, VERTICAL_RIGHT);
    Segment seg2 = Segment(93, 4, MIDDLE_RIGHT);
    Segment seg3 = Segment(97, 7, HORIZONTAL_RIGHT);
    Segment seg4 = Segment(104, 18, HORIZONTAL_MIDDLE);
    Segment seg5 = Segment(122, 7, HORIZONTAL_LEFT);
    Segment seg6 = Segment(129, 4, MIDDLE_LEFT);
    Segment seg7 = Segment(133, 93, VERTICAL_LEFT);
    
    vector<Segment> segVect;
    segVect.push_back(seg1);
    segVect.push_back(seg2);
    segVect.push_back(seg3);
    segVect.push_back(seg4);
    segVect.push_back(seg5);
    segVect.push_back(seg6);
    segVect.push_back(seg7);
    
    Strip Strip_1 = Strip(&segVect);
    
    // Position initialization
    Segment* segToSet = Strip_1.get_segment(VERTICAL_RIGHT);
    for(int i = 0; i<segToSet->get_leds()->size(); i++){
        int led_index = segToSet->get_leds()->at(i).get_id_abs();
        segToSet->set_position_loc(i, Point3f(30, 45+i*(100.0/6.0), -10));
    }
    segToSet = Strip_1.get_segment(MIDDLE_RIGHT);
    for(int i = 0; i<segToSet->get_leds()->size(); i++){
        int led_index = segToSet->get_leds()->at(i).get_id_abs();
        segToSet->set_position_loc(i, Point3f(30, 1568-i*(100.0/6.0), -10));
    }
    segToSet = Strip_1.get_segment(HORIZONTAL_RIGHT);
    for(int i = 0; i<segToSet->get_leds()->size(); i++){
        int led_index = segToSet->get_leds()->at(i).get_id_abs();
        segToSet->set_position_loc(i, Point3f(47.5+i*(100.0/6.0), 1465, -60));
    }
    segToSet = Strip_1.get_segment(HORIZONTAL_MIDDLE);
    for(int i = 0; i<segToSet->get_leds()->size(); i++){
        int led_index = segToSet->get_leds()->at(i).get_id_abs();
        segToSet->set_position_loc(i, Point3f(115-i*(100.0/6.0), 1525, -60));
    }
    segToSet = Strip_1.get_segment(HORIZONTAL_LEFT);
    for(int i = 0; i<segToSet->get_leds()->size(); i++){
        int led_index = segToSet->get_leds()->at(i).get_id_abs();
        segToSet->set_position_loc(i, Point3f(-47.5+i*(100.0/6.0), 1525, -60));
    }
    segToSet = Strip_1.get_segment(MIDDLE_LEFT);
    for(int i = 0; i<segToSet->get_leds()->size(); i++){
        int led_index = segToSet->get_leds()->at(i).get_id_abs();
        segToSet->set_position_loc(i, Point3f(-30, 1526+i*(100.0/6.0), -10));
    }
    segToSet = Strip_1.get_segment(VERTICAL_LEFT);
    for(int i = 0; i<segToSet->get_leds()->size(); i++){
        int led_index = segToSet->get_leds()->at(i).get_id_abs();
        segToSet->set_position_loc(i, Point3f(-30, 1568-i*(100.0/6.0), -10));
    }


    const auto start_time = std::chrono::high_resolution_clock::now();    
    int tick = 0;
    double tick_duration_ms = 50;


    while(1){
        Strip_1.buffer_clean(); // Optional
        double speed = 4.0;
        double width = 0.35;
        double power = 1.00;
        
        // Apply rainbow effect
        rainbow(&Strip_1, tick, speed, width, power);        
        
        // Write led colors into buffer and render
        Strip_1.buffer_write(true);   
        Strip_1.buffer_render();

        const auto now = std::chrono::high_resolution_clock::now();
        const std::chrono::duration<double, std::micro> dt_us = now - start_time;
        cout << "dt_us : " << dt_us.count() << endl;        

        double sleep_for_us = modulo(dt_us.count(), tick_duration_ms*1000);
        cout << "sleep_for : " << sleep_for_us << endl;
        auto toto = std::chrono::duration_cast<std::chrono::microseconds>(sleep_for_us * 1us);
        cout << "toto : " << toto.count() << endl;
        
        
        sleeper(toto);
        tick++;    
        cursed();
    }
    
    return 0;
}
