#include <chrono>
#include <iostream>
#include <thread>
#include "effects_global.hpp"
#include "effects.hpp"
#include "../lib/visualizer/visualizer/visualizer.cpp"

using namespace std;

void sleeper(std::chrono::microseconds us){
    auto start = std::chrono::high_resolution_clock::now();
    auto end = start + us;
    do{
        std::this_thread::yield();
    }while(std::chrono::high_resolution_clock::now() < end);
}

int run_led();

enum{
    VERTICAL_RIGHT = 0,
    VERTICAL_LEFT,
    HORIZONTAL_RIGHT,
    HORIZONTAL_MIDDLE,
    HORIZONTAL_LEFT,
    MIDDLE_RIGHT,
    MIDDLE_LEFT
};

int main(){
    std::thread thread1(&run_visualizer);
    std::thread thread2(&run_led);

    // Shitty way of doing things but works
    thread1.join();
    glfwTerminate();
    thread2.join();
    
    return 0;
}

int run_led(){
    // Loading Strip from file
    Strip Strip_1 = Strip("../structure.json");
    
    const auto start_time = std::chrono::high_resolution_clock::now();    
    int tick = 0;
    double tick_duration_ms = 50;    

    while(1){
        Strip_1.buffer_clean(); // Optional
        double speed = 4.0;
        double width = 360;
        double power = 0.33;
        
        // Apply rainbow effect
        scroll(&Strip_1, tick, speed, width, power);        
        
        // Write led colors into buffer and render
        Strip_1.buffer_write(false);   
        Strip_1.buffer_render();

        const auto now = std::chrono::high_resolution_clock::now();
        const std::chrono::duration<double, std::micro> dt_us = now - start_time;
        cout << "dt_us : " << dt_us.count() << endl;        

        double sleep_for_us = modulo(dt_us.count(), tick_duration_ms*1000);
        cout << "sleep_for : " << sleep_for_us << endl;
        auto toto = std::chrono::duration_cast<std::chrono::microseconds>(sleep_for_us * 1us);
        cout << "toto : " << toto.count() << endl;
        
        sleeper(toto);
        tick++;    
        cursed();
    }

    return 0;
}

