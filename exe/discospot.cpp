#include <chrono>
#include <iostream>
#include <thread>
#include <jsoncpp/json/json.h>
#include "effects_global.hpp"

using namespace std;

enum{
    ALL_LEDS = 0,
};

int main(void) {
    // Create a segment with all 226 leds 
    Segment seg1 = Segment(0, 226, ALL_LEDS);
    vector<Segment> segVect;
    segVect.push_back(seg1);

    // Create a Strip object from it
    Strip Strip_1(&segVect);

    // Set all the position (nor necessary)
    for(int i = 0; i<Strip_1.get_led_count(); i++){
        Strip_1.set_position_abs(i, Point3f(25, 30+i*30, 50));
    }
    
    double bpm = 143.0;
    double period = 1000.0*60.0/bpm;
    double duty_cycle = 0.8;
    const auto start = std::chrono::high_resolution_clock::now();

    bool led_on;
    bool playing = false;
    popen("python3 testfile.py", "r");

    while(1){
        auto start_time = std::chrono::high_resolution_clock::now();
        // Apply effect
        led_on = blink(&Strip_1);        
        // Render
        Strip_1.buffer_render();        
        
        period = 1000.0*60.0/bpm;

        const auto now = std::chrono::high_resolution_clock::now();
        const std::chrono::duration<double, std::milli> dt_ms = now - start_time;
        std::cout << "dt_us : " << dt_ms.count() << endl;        
        double sleep_for = 0;
        if(led_on){
            sleep_for = period*duty_cycle-dt_ms.count();

            Json::Value vars;
                do{
                    std::cout << "trying.." << endl;
                    std::ifstream flux("test.json", std::ifstream::binary);                    
                    flux >> vars;
                }while(vars.isNull());
                              
                bpm = vars["bpm"].asDouble();
                playing = vars["playing"].asBool();
                std::cout << "Bpm : " << bpm << "\tPlaying : " << playing << std::endl; 
        }
        else{
            sleep_for = period*(1-duty_cycle)-dt_ms.count();
        }
        
        std::cout << "sleep_for : " << sleep_for << endl;
        auto toto = std::chrono::duration_cast<std::chrono::milliseconds>(sleep_for * 1ms);
        std::cout << "toto : " << toto.count() << endl;
        std::this_thread::sleep_for(toto);
        cursed();
    }
    
    return 0;
}
