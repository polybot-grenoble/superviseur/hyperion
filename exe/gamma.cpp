#include <chrono>
#include <iostream>
#include <thread>
#include "Strip.hpp"

using namespace std;

enum{
    VERTICAL_RIGHT = 0,
    VERTICAL_LEFT,
};

int main(void) {
    Segment seg1 = Segment(0, 93, VERTICAL_RIGHT);
    Segment seg7 = Segment(133, 93, VERTICAL_LEFT);

    LedStrip *strip = init_led_strip();
    
    clean_strip(strip);      

    // Compute gradient
    double power = 1.00;
    
    for(auto& led : *seg1.get_leds()){
        double color = led.get_id_abs()*360.0/93.0;
        led.hsv = Color(color, 1, power);         
    }
    for(auto& led : *seg7.get_leds()){
        double color = (1-(led.get_id_abs()-133+1)/93.0)*360.0;
        led.hsv = Color(color, 1, power);         
    }
    
    // Write values to buffer
    for(auto led : *seg1.get_leds()){
        set_buffer_HSV_strip(led.hsv.h, led.hsv.s, led.hsv.v, led.get_id_abs(), strip->buffer_data, strip->ptr_lut, true);
    }
    for(auto led : *seg7.get_leds()){
        set_buffer_HSV_strip(led.hsv.h, led.hsv.s, led.hsv.v, led.get_id_abs(), strip->buffer_data, strip->ptr_lut, false);
    }      

    render_strip(strip);    
    destroy_led_strip(strip);
    
    return 0;
}
