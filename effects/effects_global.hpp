#ifndef __HYPERION_EFFECTS_EFFECTS__
#define __HYPERION_EFFECTS_EFFECTS__

#include "Strip.hpp"

void rainbow(Strip *strip, int tick, double speed, double width, double power);

void scroll(Strip *strip, int tick, double speed, double width, double power);

bool blink(Strip *strip);

#endif