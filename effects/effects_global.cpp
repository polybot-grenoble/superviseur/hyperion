#include "effects_global.hpp"

void rainbow(Strip *strip, int tick, double speed, double width, double power){
    double color;
    // For all leds, calculate color based on vertical position, rolling with time
    vector<Segment>* ptr_segs = strip->get_segments();   
    for(auto& seg : *ptr_segs){
        // vector<Led>* ptr_leds = seg.get_leds();        
        // for(auto& led : *ptr_leds){
        //     color = modulo(tick*speed+led.get_position().y*width, 360.0);
        //     led.hsv = Color(color, 1, power);
        // }
        seg.effect_rainbow(tick, speed, width, power);
    }             
}

void scroll(Strip *strip, int tick, double speed, double width, double power){
    double color;
    // For all leds, calculate color based on vertical position, rolling with time
    vector<Segment>* ptr_segs = strip->get_segments();   
    for(auto& seg : *ptr_segs){
        seg.effect_scroll(tick, speed, width, power);
    }             
}

bool blink(Strip *strip){
    static int iter = 0;
    static bool ledOn = true;
    // Either change color or turn off
    if(ledOn){
        for(int i = 0; i<strip->get_led_count(); i++){
            double color = modulo(iter*137.51, 360.0);
            strip->set_color_abs(i, Color(color, 1, 1));
        }
        strip->buffer_write(false);
        iter++;
    }
    else{
        strip->buffer_clean();
    }
    ledOn = !ledOn;
    return ledOn;
}
