#ifndef __HYPERION_EFFECTS_STRIP__
#define __HYPERION_EFFECTS_STRIP__

#include "../driver/driver.hpp"
#include "../math/functions.hpp"
#include "../includes/json.hpp"
#include <vector>
#include <string>
#include <fstream>

using namespace std;
using json = nlohmann::json;

class Led{
    public:
        Color hsv;  // Color of the LED in HSV space

        Led();
        
        /**
         * @brief Constructor - Construct from absolute and local ids and position
         * @param id_abs Absolute id - The physical position of the led along the strip
         * @param id_loc Local id - An arbitrary id to reference the led in its segment. Local ids must be unique and continuous inside a given segment
         * @param position Spacial position of the led - Can be changed later
         * @param curveu Position of the led along a curve, ranging from 0 to 1 
         */
        Led(int id_abs, int id_loc, Point3f position, float curveu);
        
        friend ostream& operator<<(ostream& os, const Led& led);
        
        /**
         * @brief Set the position of led
         * @param pos Desired position of the led
         */
        void set_position(Point3f pos);
        
        /**
         * @brief Get the position of the led
         * @return Position of the led
         */
        Point3f get_position();

        /**
         * @brief Get the absolute id of the led
         * @return Absolute id of the led
         */
        int get_id_abs();
        
        /**
         * @brief Get the local id of the led
         * @return Local id of the led
         */
        int get_id_loc();


        /**
         * @brief Get the curveu of the led
         * @return Curveu of the led
         */
        float get_curveu();

    
    private:
        int id_abs; // Absolute id of the led
        int id_loc; // Local id of the led
        Point3f position;   // Position of the led
        float curveu; // Position of the led along a curve
};

class Segment{
    public:          
        Segment();
        
        /**
         * @brief Constructor - Construct from a vector of leds and a segment id
         * @param leds Vector of leds
         * @param id Segment id - Must be unique and continuous for segments in a strip
         */
        Segment(vector<Led> leds, int id);
        
        /**
         * @brief Constructor - Construct from a given length, a starting led and a segment id
         * @param start Absolute index of the first led
         * @param size number of leds
         * @param id Segment id - Must be unique and continuous for segments in a strip
         */
        Segment(int start, int size, int id);

        friend ostream& operator<<(ostream& os, const Segment& segment);

        /**
         * @brief Set the position of a led using its absolute id
         * @param id_abs Absolute id of the led
         * @param pos Desired position of the led
         * @note Not implemented yet
         */
        void set_position_abs(int id_abs, Point3f pos);

        /**
         * @brief Set the position of a led using its local id
         * @param id_loc Local id of the led
         * @param pos Desired position of the led
         */
        void set_position_loc(int id_loc, Point3f pos);

        /**
         * @brief Set the color of a led using its absolute id
         * @param id_abs Absolute id of the led
         * @param hsv Desired color in HSV space
         * @note Not implemented yet
         */
        void set_color_abs(int id_abs, Color hsv);

        /**
         * @brief Set the color of a led using its local id
         * @param id_loc Local id of the led
         * @param hsv Desired color in HSV space
         * @note Not implemented yet
         */
        void set_color_loc(int id_loc, Color hsv);

        /**
         * @brief Get the id of the segment
         * @return Id of the segment
         */
        int get_id();

        /**
         * @brief Get the number of leds contained in the segment
         * @return Number of led contained in the segment
         */
        int get_size();

        /**
         * @brief Return a pointer to the vector of led, allowing direct acces to individual leds
         * @return Pointer to the vector of leds
         * @warning The vector must NEVER be resized. Elements must NEVER be appended, removed, or swapped around 
         */
        vector<Led>* get_leds();

        void effect_rainbow(int tick, double speed, double width, double power);
        void effect_scroll(int tick, double speed, double width, double power);

    private:
        vector<Led> leds_vect;  // Vector of leds
        int id; // Id of the segment
};

class Strip{
    public:
        LedStrip* strip_buffer; // Pointer to the struct holding what is necessarie to render the strip
        vector<Led*> leds_indexer;  // Vector of Led pointer allowing direct acces to led by absolute index at the Strip level
        
        /**
         * @brief Constructor - Construct from a pointer to a vector of segments
         * @param Pointer to a vector of segments
         */
        Strip(vector<Segment>* segments);

        /**
         * @brief Constructor - Construct from a file containing the structure of the leds
         * @param path Path to the .json file containing the structure
         */
        Strip(string path);

        ~Strip();

        /**
         * @brief Return a pointer to the segment selected by id, allowing direct acces to the segment
         * @param id Id of the segment to acces
         * @return Pointer to the selected segment
         */
        Segment* get_segment(int id);

        /**
         * @brief Return a pointer to the vector of segments, allowing direct acces to individual segments
         * @return Pointer to the vector of segments
         * @warning The vector must NEVER be resized. Elements must NEVER be appended, removed, or swapped around 
         */
        vector<Segment>* get_segments();
        
        /**
         * @brief Return the total number of leds in the strip
         * @return Number of ledes in the strip
         */
        int get_led_count();

        /**
         * @brief Set the position of a led using its absolute id
         * @param id_abs Absolute id of the led
         * @param pos Desired position of the led
         * @note Not implemented yet
         */
        void set_position_abs(int id_abs, Point3f pos);

        /**
         * @brief Set the color of a led using its absolute id
         * @param id_abs Absolute id of the led
         * @param hsv Desired color in HSV space
         * @note Not implemented yet
         */
        void set_color_abs(int id_abs, Color hsv);

        /**
         * @brief Wrapper to write the leds color data into the data buffer
         * @param doGamma Whether to use gamma correction or not
         */
        void buffer_write(bool doGamma);        
        
        /**
         * @brief Wrapper to reset the data buffer to zero - Resets the data buffer by copying a clean buffer into it
         */
        void buffer_clean();

        /**
         * @brief Wrapper to render the data buffer - Data must be first copied into it with buffer_write()
         */
        void buffer_render();

        /**
         * @brief Wrapper to print the full data buffer in hexadecimal
         */
        void buffer_print();

    private:
        int led_count;  // Number of leds in the strip
        vector<Segment> segments_vect;  // Vector of segments

        /**
         * @brief Checks that all the segments are valid before constructing the strip
         * @param _segments Pointer to the vector of segments to verify
         */
        void validate_segments(vector<Segment>* _segments);

        /**
         * @brief Checks that all the leds in all the segments are valid
         * @return Number of leds in the strip
         */
        int validate_leds();

        /**
         * @brief Build the vector of pointers to Led allowing to acces led by absolute id at the strip level
         */
        void construct_indexer();
};



#endif
