#include "effects.hpp"

void Segment::effect_rainbow(int tick, double speed, double width, double power){    
    double color;        
    vector<Led>* ptr_leds = this->get_leds();        
    for(auto& led : *ptr_leds){
        color = modulo(tick*speed+led.get_position().y*width, 360.0);
        led.hsv = Color(color, 1, power);
    }           
}

void Segment::effect_scroll(int tick, double speed, double width, double power){
    double color;
    vector<Led>* ptr_leds = this->get_leds();
    for(auto& led : *ptr_leds){
        color = modulo(tick*speed+led.get_curveu()*width, 360.0);
        led.hsv = Color(color, 1, power);
    }
}