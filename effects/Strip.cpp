#include "Strip.hpp"

// #####################     LED     #####################
Led::Led(){
    this->id_abs = -1;
    this->id_loc = -1;
    this->position = Point3f(0.0, 0.0, 0.0);
    this->hsv = Color(0.0, 0.0, 0.0);
    this->curveu = 0.0;
}

Led::Led(int id_loc, int id_abs, Point3f position, float curveu){
    this->id_abs = id_abs;
    this->id_loc = id_loc;
    this->position = position;
    this->hsv = Color(0.0, 0.0, 0.0);
    this->curveu = curveu;
}

void Led::set_position(Point3f pos){
    position = pos;
}

Point3f Led::get_position(){
    return position;
}

int Led::get_id_abs(){
    return id_abs;
}

int Led::get_id_loc(){
    return id_loc;
}

float Led::get_curveu(){
    return curveu;
}

ostream& operator<<(ostream& os, const Led& led){
    os << "\tINDEX : " << led.id_abs << "\n\t" << led.position << "\n\t" << led.hsv << endl;
    return os;
}


// #####################   SEGMENT   #####################
Segment::Segment(){}

Segment::Segment(vector<Led> leds, int id){
    this->id = id;
    this->leds_vect.insert( end(leds_vect), begin(leds), end(leds) );
}

Segment::Segment(int start, int size, int id){
    this->id = id;
    for(int i = 0; i<size; i++){
        this->leds_vect.push_back(Led(i, i+start, Point3f(0.0, 0.0, 0.0), 0.0));
    }
}

void Segment::set_position_abs(int id_abs, Point3f pos){
    cout << "Not implemented..." << endl;
}

void Segment::set_position_loc(int id_loc, Point3f pos){
    this->leds_vect[id_loc].set_position(pos);
}

void Segment::set_color_abs(int id_abs, Color hsv){
    cout << "Not implemented..." << endl;
}

void Segment::set_color_loc(int id_loc, Color hsv){
    cout << "Not implemented..." << endl;
}

int Segment::get_id(){
    return id;
}

int Segment::get_size(){
    return leds_vect.size();
}

vector<Led>* Segment::get_leds(){
    return &leds_vect;
}

ostream& operator<<(ostream& os, const Segment& segment){
    for(int i = 0; i< segment.leds_vect.size() ; i++ ){
        os << segment.leds_vect[i] << endl;
    };
    return os;
}


// #####################    STRIP    #####################
Strip::Strip(vector<Segment>* _segments){
    led_count = -1;

    cout << "##### Constructing strip..." << endl;
    // Validates the segments
    validate_segments(_segments); 

    // Store the segments in order
    segments_vect.resize(_segments->size());
    for(int i = 0; i<_segments->size(); i++){
        segments_vect.at(_segments->at(i).get_id()) = _segments->at(i);
    }

    // Validate leds
    led_count = validate_leds();

    // Construct led indexer
    construct_indexer();

    // Initialize the buffer strip
    strip_buffer = init_led_strip();

    cout << "##### Strip succesfully constructed!" << endl;
}

Strip::Strip(string path){
    // Parse the json file
    ifstream fJson("../structure.json");
    stringstream buffer;
    buffer << fJson.rdbuf();
    auto json = json::parse(buffer.str());

    // Loading basic attributes (unused)
    int seg_number = json["seg_number"];
    int led_number = json["led_number"];

    vector<Segment> seg_vect;
    // Build segment vector
    for(auto seg : json["segments"]){
        int id = seg["id"];
        string name = seg["name"];
        int size = seg["size"];
        
        // Build led vector
        vector<Led> leds;
        for(auto led : seg["leds"]){
            int id_loc = led["id_loc"];
            int id_abs = led["id_abs"];
            Point3f pos =  Point3f(led["pos"][0], led["pos"][1], led["pos"][2]);
            float curveu = led["curveu"];
            Led new_led = Led(id_loc, id_abs, pos, curveu);
            leds.push_back(new_led);
        }

        Segment new_segment = Segment(leds, id);
        seg_vect.push_back(new_segment);
    }

    led_count = -1;

    cout << "##### Constructing strip..." << endl;
    // Validates the segments
    validate_segments(&seg_vect); 

    // Store the segments in order
    segments_vect.resize(seg_vect.size());
    for(int i = 0; i<seg_vect.size(); i++){
        segments_vect.at(seg_vect.at(i).get_id()) = seg_vect.at(i);
    }

    // Validate leds
    led_count = validate_leds();

    // Construct led indexer
    construct_indexer();

    // Initialize the buffer strip
    strip_buffer = init_led_strip();

    cout << "##### Strip succesfully constructed!" << endl;
}

Strip::~Strip(){
    destroy_led_strip(strip_buffer);
}

Segment* Strip::get_segment(int id){
    if(id<0 || id>=segments_vect.size()){
        cerr << "[ERROR] While getting segment : id=" << id << " (out of bounds)" << endl;
        exit(1);
    }
    return &(segments_vect[id]);
}

vector<Segment>* Strip::get_segments(){
    return &segments_vect;
}

int Strip::get_led_count(){
    return led_count;
}

void Strip::set_position_abs(int id_abs, Point3f pos){
    if(id_abs<0 || id_abs>=this->led_count){
        cerr << "[ERROR] While accesing led : id_abs=" << id_abs << " (out of bounds)" << endl;
        exit(1);
    }
    leds_indexer[id_abs]->set_position(pos);
}

void Strip::set_color_abs(int id_abs, Color hsv){
    if(id_abs<0 || id_abs>=this->led_count){
        cerr << "[ERROR] While accesing led : id_abs=" << id_abs << " (out of bounds)" << endl;
        exit(1);
    }
    leds_indexer[id_abs]->hsv = hsv;
}

void Strip::validate_segments(vector<Segment>* _segments){
    cout << "Validating segments..." << endl;
    vector<bool> idExists(_segments->size(), false);
    
    // Check that segment IDs are unique and continuous starting from 0
    for(int i = 0; i<_segments->size(); i++){
        if(_segments->at(i).get_id() < 0){
            cerr << "[ERROR] While validating segment: found id=" << _segments->at(i).get_id() << " (negative id)" << endl;
            exit(1);
        }
        if(_segments->at(i).get_id() >= _segments->size()){
            cerr << "[ERROR] While validating segment : found id=" << _segments->at(i).get_id() << " (>= number of segments " << _segments->size() << ")" << endl;
            exit(1);
        }
        if(idExists[_segments->at(i).get_id()]){
            cerr << "[ERROR] While validating segment : found id=" << _segments->at(i).get_id() << " (duplicate id)" << endl;
            exit(1);
        }
        else{
            idExists[_segments->at(i).get_id()] = true;
        }
    }
    cout << "All segments validated!" << endl;
}

int Strip::validate_leds(){
    cout << "Validation leds: Beginning validation..." << endl;
    // [1/2] Check that all LEDs id_abs are unique, continuous and within bounds
    cout << "Validation leds: Checking that id_abs are unique, continuous and within bounds..." << endl;
    vector<bool> idAbsExists(STRIP_SIZE, false);
    int ledCount = 0;
    // Check bounds and uniqueness
    for(auto segment : this->segments_vect){    // For each segment:
        vector<Led>* ptr_leds = segment.get_leds();
        for(auto led : *ptr_leds){  // For each led:
            if(led.get_id_abs() < 0){
                cerr << "[ERROR] While validating strip : found id_abs=" << led.get_id_abs() << " (negative id)" << endl; 
                exit(1);
            }
            if(led.get_id_abs() >= STRIP_SIZE){
                cerr << "[ERROR] While validating strip : found id_abs=" << led.get_id_abs() << " (>= defined strip size " << STRIP_SIZE << ")" << endl;
                exit(1);
            }
            if(idAbsExists[led.get_id_abs()]){
                cerr << "[ERROR] While validating strip : found id_abs=" << led.get_id_abs() << " (duplicate id)" << endl; 
                exit(1);
            }
            else{
                idAbsExists[led.get_id_abs()] = true;
                ledCount++;
            }
        }
    }
    
    // Check continuousness
    bool endReached = false;
    for(int i = 0; i<idAbsExists.size(); i++){        
        if(idAbsExists[i] == false && endReached == false){
            if(i == ledCount){
                endReached = true;  // End of physical strip reached 
            }
            else{
                cerr << "[ERROR] While validating strip : strip is not continous at id_abs=" << i << " (hole before)" << endl; 
                exit(1);            
            }            
        }
        if(idAbsExists[i] == true && endReached == true){
            cerr << "[ERROR] While validating strip : strip is not continous at id_abs=" << i << " (hole after)" << endl; 
            exit(1);
        }
    }
    cout << "Validation leds: All id_abs are unique, continuous and within bounds!" << endl;

    // [2/2] Check that all LEDs id_loc are unique, continuous and within bounds inside each segment
    cout << "Validation leds: Checking that id_loc are unique, continuous and within bounds..." << endl;    
    // Check bounds and uniqueness (continuousness is implicit)
    for(auto segment : this->segments_vect){    // For each segment:
        vector<Led>* ptr_leds = segment.get_leds();
        vector<bool> idLocExists(ptr_leds->size(), false);
        int ledCountSeg = 0;
        for(auto led : *ptr_leds){  // For each led:
            if(led.get_id_loc() < 0){
                cerr << "[ERROR] While validating strip : found id_loc=" << led.get_id_loc() << " (negative id)" << endl; 
                exit(1);
            }
            if(led.get_id_loc() >= ptr_leds->size()){
                cerr << "[ERROR] While validating strip : found id_loc=" << led.get_id_loc() << " (>= segment size " << ptr_leds->size() << ")" << endl;
                exit(1);
            }
            if(idLocExists[led.get_id_loc()]){
                cerr << "[ERROR] While validating strip : found id_loc=" << led.get_id_loc() << " (duplicate id)" << endl; 
                exit(1);
            }
            else{
                idLocExists[led.get_id_loc()] = true;
                ledCountSeg++;
            }
        }
    }
    cout << "Validation leds: All id_loc are unique, continuous and within bounds!" << endl;    

    return ledCount;
}

void Strip::construct_indexer(){
    // Build led indexer
    cout << "Constructing led indexer..." << endl;  
    leds_indexer.resize(led_count);
    vector<Led>* ptr_leds;
    for(int i = 0; i<segments_vect.size(); i++){
        ptr_leds = segments_vect[i].get_leds();
        for(int j = 0; j<ptr_leds->size(); j++){
            leds_indexer[ptr_leds->at(j).get_id_abs()] = &(ptr_leds->at(j));    // Write the adress of the led at index equal to the led abs_id 
        }
    } 
    cout << "Led indexer sucessfully constructed!" << endl;
}

void Strip::buffer_write(bool doGamma){
    for(auto segment : segments_vect){
        vector<Led>* ptr_leds = segment.get_leds();
        for(auto led : *ptr_leds){
            int id = led.get_id_abs();
            set_buffer_HSV_strip(led.hsv.h, led.hsv.s, led.hsv.v, id, strip_buffer->buffer_data, strip_buffer->ptr_lut, doGamma);
        }
    }
}

void Strip::buffer_clean(){
    clean_strip(strip_buffer);
}

void Strip::buffer_render(){
    render_strip(strip_buffer);
}

void Strip::buffer_print(){
    print_data_buffer(strip_buffer);
}
