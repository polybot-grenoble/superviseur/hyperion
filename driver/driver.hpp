#ifndef __HYPERION_DRIVER_DRIVER__
#define __HYPERION_DRIVER_DRIVER__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include <math.h>
#include <time.h>

#define SPI_DEVICE          "/dev/spidev0.0"
#define STRIP_SIZE 1000
#define MAX_SIZE 1100

typedef struct LedStrip{
    int spi_fd;
    int ret;
    uint8_t mode;
    uint8_t bits;
    uint32_t speed;
    uint8_t *ptr_lut;
    uint8_t buffer_clean[MAX_SIZE*9];
    uint8_t buffer_data[MAX_SIZE*9];
    struct spi_ioc_transfer tr;
}LedStrip;

/**
 * @brief Initialize a LedStrip structure
*/
LedStrip* init_led_strip();

/**
 * @brief Destroy a LedStrip structure
*/
void destroy_led_strip(LedStrip *strip);

/**
 * @brief Initialize a LUT containing all the possible bit patterns
*/
uint8_t* init_lut();

/**
 * @brief Render a LedStrip by sending the data buffer to the SPI  
*/
void render_strip(LedStrip *strip);

/**
 * @brief Clean the data buffer by copying the clean buffer into it
*/
void clean_strip(LedStrip *strip);

/**
 * @brief Set a buffer to a RGB color using the LUT
*/
void set_buffer_RGB(uint8_t R, uint8_t G, uint8_t B, uint8_t *buffer, uint8_t *ptr_lut);

/**
 * @brief Set a buffer to a RGB color using the LUT with gamma correction
*/
void set_buffer_RGB_Gamma(uint8_t R, uint8_t G, uint8_t B, uint8_t *buffer, uint8_t *ptr_lut);

/**
 * @brief Set a specific led to a RGB color in a buffer using the LUT
*/
void set_buffer_RGB_strip(uint8_t R, uint8_t G, uint8_t B, int led, uint8_t *buffer, uint8_t *ptr_lut);

/**
 * @brief Set a buffer to a HSV color using the LUT
*/
void set_buffer_HSV(float H, float S, float V, uint8_t *buffer, uint8_t *ptr_lut, bool doGamma = false);

/**
 * @brief Set a specific led to a HSV color in a buffer using the LUT
*/
void set_buffer_HSV_strip(float H, float S, float V, int led, uint8_t *buffer, uint8_t *ptr_lut, bool doGamma = false);

/**
 * @brief Prints the full data buffer in hexadecimal
 */
void print_data_buffer(LedStrip *strip);

void cursed();

#endif
