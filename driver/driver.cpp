#include "driver.hpp"

LedStrip* init_led_strip(){
    LedStrip *strip = (LedStrip*)malloc(sizeof(LedStrip));
    if(strip == NULL){
        fprintf(stderr, "[ERROR] Cannot allocate LedStrip\n");
        exit(1);
    }
    strip->bits = 8;
    strip->speed = 2500000;
    
    // Open SPI peripheral
    strip->spi_fd = open("/dev/spidev0.0", O_RDWR);
    if (strip->spi_fd < 0) fprintf(stderr, "[WARNING] Cannot open /dev/spidev0.0\n");
    
    // Check SPI mode
    if (ioctl(strip->spi_fd, SPI_IOC_WR_MODE, &strip->mode) < 0) fprintf(stderr, "[WARNING] Cannot write SPI mode\n");
    if (ioctl(strip->spi_fd, SPI_IOC_RD_MODE, &strip->mode) < 0) fprintf(stderr, "[WARNING] Cannot read SPI mode\n");
    // Check Bits per word
    if (ioctl(strip->spi_fd, SPI_IOC_WR_BITS_PER_WORD, &strip->bits) < 0) fprintf(stderr, "[WARNING] Cannot write SPI bit per word\n");
    if (ioctl(strip->spi_fd, SPI_IOC_RD_BITS_PER_WORD, &strip->bits) < 0) fprintf(stderr, "[WARNING] Cannot read SPI bit per word\n");
    // Check Max speed Hz
    if (ioctl(strip->spi_fd, SPI_IOC_WR_MAX_SPEED_HZ, &strip->speed) < 0) fprintf(stderr, "[WARNING] Cannot write SPI speed\n");
    if (ioctl(strip->spi_fd, SPI_IOC_RD_MAX_SPEED_HZ, &strip->speed) < 0) fprintf(stderr, "[WARNING] Cannot read SPI speed\n");
    
    // Allocate LUT 
    strip->ptr_lut =  init_lut();
    if(strip->ptr_lut == NULL){
        fprintf(stderr, "[ERROR] Cannot allocate LUT\n");
        exit(1);
    } 
    
    for(int i = 0; i<MAX_SIZE*9; i++){
        strip->buffer_clean[i] = 0;
    }
    // Initialize a clean buffer
    for(int i = 0; i<STRIP_SIZE; i++){
        set_buffer_RGB_strip(0, 0, 0, i, strip->buffer_clean, strip->ptr_lut);
    }
    
    // Duplicate clean buffer into data buffer
    memcpy(strip->buffer_data, strip->buffer_clean, MAX_SIZE);
    
    // Initialize the SPI transfer
    memset(&strip->tr, 0, sizeof(struct spi_ioc_transfer));
    strip->tr.tx_buf = (unsigned long)strip->buffer_data;
    strip->tr.len = MAX_SIZE*9;
    
    return strip;
}

void destroy_led_strip(LedStrip *strip){
    free(strip->ptr_lut);
    close(strip->spi_fd);
    free(strip);
}

uint8_t* init_lut(){
    uint8_t *ptr_lut = (uint8_t*)malloc(sizeof(uint8_t)*3*256);
    if(ptr_lut == NULL) return NULL;
    for(int i = 0; i<256; i++){        
        ptr_lut[0 + i*3] = 0b10010010 | (((i>>(7-0))&1)<<6) | (((i>>(7-1))&1)<<3) | (((i>>(7-2))&1)<<0);
        ptr_lut[1 + i*3] = 0b01001001 | (((i>>(7-3))&1)<<5) | (((i>>(7-4))&1)<<2);
        ptr_lut[2 + i*3] = 0b00100100 | (((i>>(7-5))&1)<<7) | (((i>>(7-6))&1)<<4) | (((i>>(7-7))&1)<<1);    
    }
    return ptr_lut;
}

void render_strip(LedStrip *strip){
    strip->ret = ioctl(strip->spi_fd, SPI_IOC_MESSAGE(1), &strip->tr);
    if (strip->ret < 1) fprintf(stderr, "[WARNING] Can't send spi buffer\n");
}

void clean_strip(LedStrip *strip){
    memcpy(strip->buffer_data, strip->buffer_clean, 9*STRIP_SIZE);
}

void set_buffer_RGB(uint8_t R, uint8_t G, uint8_t B, uint8_t *buffer, uint8_t *ptr_lut){ 
    buffer +=20;
    buffer[0] = ptr_lut[G*3 + 0];
    buffer[1] = ptr_lut[G*3 + 1];
    buffer[2] = ptr_lut[G*3 + 2];
    buffer[3] = ptr_lut[R*3 + 0];
    buffer[4] = ptr_lut[R*3 + 1];
    buffer[5] = ptr_lut[R*3 + 2];
    buffer[6] = ptr_lut[B*3 + 0];
    buffer[7] = ptr_lut[B*3 + 1];
    buffer[8] = ptr_lut[B*3 + 2];
}

void set_buffer_RGB_Gamma(uint8_t R, uint8_t G, uint8_t B, uint8_t *buffer, uint8_t *ptr_lut){ 
    buffer +=20;
    const float gamma = 2.2;
    R = (uint8_t)(pow((float)R/255.0, gamma)*255);
    G = (uint8_t)(pow((float)G/255.0, gamma)*255);
    B = (uint8_t)(pow((float)B/255.0, gamma)*255);
    
    buffer[0] = ptr_lut[G*3 + 0];
    buffer[1] = ptr_lut[G*3 + 1];
    buffer[2] = ptr_lut[G*3 + 2];
    buffer[3] = ptr_lut[R*3 + 0];
    buffer[4] = ptr_lut[R*3 + 1];
    buffer[5] = ptr_lut[R*3 + 2];
    buffer[6] = ptr_lut[B*3 + 0];
    buffer[7] = ptr_lut[B*3 + 1];
    buffer[8] = ptr_lut[B*3 + 2];
}

void set_buffer_HSV(float H, float S, float V, uint8_t *buffer, uint8_t *ptr_lut, bool doGamma){
    float R, G, B;
    int H_i = (int)floorf(H/60.0)%6;
    
    float F = H/60.0 - H_i;
    float L = V*(1-S);
    float M = V*(1-F*S);
    float N = V*(1-(1-F)*S);
    switch(H_i){
        case 0 :
            R = V;
            G = N;
            B = L;
            break;
        case 1 :
            R = M;
            G = V;
            B = L;
            break;
        case 2 :
            R = L;
            G = V;
            B = N;
            break;
        case 3 :
            R = L;
            G = M;
            B = V;
            break;
        case 4 :
            R = N;
            G = L;
            B = V;
            break;
        case 5 :
            R = V;
            G = L;
            B = M;
            break;
    }
    R *=255;
    G *=255;
    B *=255;
    if(doGamma){
        set_buffer_RGB_Gamma((uint8_t)R,(uint8_t)G,(uint8_t)B,buffer,ptr_lut);
    }
    else{
        set_buffer_RGB((uint8_t)R,(uint8_t)G,(uint8_t)B,buffer,ptr_lut); 
    }
}

void set_buffer_RGB_strip(uint8_t R, uint8_t G, uint8_t B, int led, uint8_t *buffer, uint8_t *ptr_lut){
    set_buffer_RGB(R, G, B, &(buffer[led*9]), ptr_lut);
}

void set_buffer_HSV_strip(float H, float S, float V, int led, uint8_t *buffer, uint8_t *ptr_lut, bool doGamma){
    set_buffer_HSV(H, S, V, &(buffer[led*9]), ptr_lut, doGamma);
}

void print_data_buffer(LedStrip *strip){
    for(int i = 0; i<MAX_SIZE; i++){
        printf("\nBlock %d:\t",i);
        printf("\tR: %02x%02x%02x", strip->buffer_data[9*i+0], strip->buffer_data[9*i+1], strip->buffer_data[9*i+2]);
        printf("\tG: %02x%02x%02x", strip->buffer_data[9*i+3], strip->buffer_data[9*i+4], strip->buffer_data[9*i+5]);
        printf("\tB: %02x%02x%02x", strip->buffer_data[9*i+6], strip->buffer_data[9*i+7], strip->buffer_data[9*i+8]);
    }
    printf("\n");
}

void cursed(){
}
